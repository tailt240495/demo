package firstday.tailt.pixastudio.Presenter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import firstday.tailt.pixastudio.Adapter.ImageAdapter;
import firstday.tailt.pixastudio.Model.APIMovie;
import firstday.tailt.pixastudio.Model.ChildMovie;
import firstday.tailt.pixastudio.Model.Movie;
import firstday.tailt.pixastudio.demomoviedatabase.R;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by tai on 09/10/2016.
 */

public class Presenter {
    private Context context;
    RecyclerView recyclerView;
    String url = "http://api.themoviedb.org/3/";
    String apikey = "5446ca07c8dacacf459775023ac5c116";
    private static final String TAG = Presenter.class.getSimpleName();
    public Presenter(Context context, RecyclerView recyclerViewC) {
        this.context = context;
        recyclerView = recyclerViewC;
    }
    public void getData(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        APIMovie service = retrofit.create(APIMovie.class);
        service.getTopRatedMovies(apikey).enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Response<Movie> response, Retrofit retrofit) {
                ArrayList<ChildMovie> movies = response.body().getResults();
                Log.d(TAG,"Count Movie:"+ movies.size());
                recyclerView.setAdapter(new ImageAdapter(movies, R.layout.activity_row, context));
            }
            @Override
            public void onFailure(Throwable t) {

            }
        });
    }
}
