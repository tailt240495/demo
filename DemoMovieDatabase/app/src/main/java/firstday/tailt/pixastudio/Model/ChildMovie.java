package firstday.tailt.pixastudio.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tai on 09/10/2016.
 */

public class ChildMovie {

    private String backdrop_path;

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }
}
