package firstday.tailt.pixastudio.Model;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by tai on 09/10/2016.
 */

public interface APIMovie {
    @GET("movie/top_rated")
    Call<Movie> getTopRatedMovies(@Query("api_key") String apiKey);
}
