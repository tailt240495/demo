package firstday.tailt.pixastudio.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tai on 09/10/2016.
 */

public class Movie {
    @SerializedName("page")
    private int page;
    @SerializedName("results")
    private ArrayList<ChildMovie> results;
    @SerializedName("total_results")
    private int totalResults;
    @SerializedName("total_pages")
    private int totalPages;
    public int getPage() {
        return page;
    }
    public void setPage(int page) {
        this.page = page;
    }
    public ArrayList<ChildMovie> getResults() {
        return results;
    }
    public void setResults(ArrayList<ChildMovie> results) {
        this.results = results;
    }
    public int getTotalResults() {
        return totalResults;
    }
    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
    public int getTotalPages() {
        return totalPages;
    }
    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
