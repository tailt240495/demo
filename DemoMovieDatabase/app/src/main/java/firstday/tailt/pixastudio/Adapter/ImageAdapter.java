package firstday.tailt.pixastudio.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import firstday.tailt.pixastudio.Model.ChildMovie;
import firstday.tailt.pixastudio.demomoviedatabase.R;

/**
 * Created by tai on 09/10/2016.
 */

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ViewHolder> {
    ArrayList<ChildMovie> movies = new ArrayList<>();
    private int rowlayout;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView img;

        public ViewHolder(View v) {
            super(v);
            img = (ImageView) v.findViewById(R.id.iv_Avatar);
        }
    }

    public ImageAdapter(ArrayList<ChildMovie> movies,int rowlayout,Context context){
        this.movies = movies;
        this.rowlayout = rowlayout;
        this.context = context;
    }
    @Override
    public ImageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowlayout,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Glide.with(context)
                .load("http://image.tmdb.org/t/p/w500"+movies.get(position).getBackdrop_path())
                .into(holder.img);
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }
}
