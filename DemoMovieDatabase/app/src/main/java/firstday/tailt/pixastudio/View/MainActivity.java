package firstday.tailt.pixastudio.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import firstday.tailt.pixastudio.Presenter.Presenter;
import firstday.tailt.pixastudio.demomoviedatabase.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_Movie);

        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this,2));

        Presenter presenter = new Presenter(this,recyclerView);
        presenter.getData();
    }
}
